"""
    Scripting
    
    2810ICT Software Technologies
    
    School of ICT Griffith University Trimester 2, 2018
    
    Due at the start of Week 7, Monday 27th August, 9am
    
    Alexander Baumgartner
    S2778992
    
"""

import re
import os

def same(item, target):
    """
    Function compares each letter of 'item' to each letter of 'target' and returns the number of letters that the two parameters have in common
    """
    return len([c for (c, t) in zip(item, target) if c == t])


def build(pattern, words, seen, list):
    """
    Function returns 'words' that match the 'pattern', which changes each time it's evoked and replaces the letter with '.' (e.g. gold --> .old, g.ld, go.d, gol.), creating a 'list' of words matching the 'pattern' which are not found in 'seen' and 'list' 
    """
    return [word for word in words
            if re.search(pattern, word) and word not in seen.keys() and word not in list]


def find(start, words, seen, target, path):
    """
    Function recursively searches for 'words' to get closer to the 'target' and returns True if found or False if no path is found.
    """
    # Checks if the length of path is specified.
    if chose_length == 'y':
        # Checks if the length of path matches the user input
        if len(path) == path_length:
            # Prints the length of path if start word matches target word.
            if start == target:
                print(len(path))
                return True
            else:
                return False
        else:
            if start == target:
                return False
    elif start == target:
        return True
    
    commonletters = []
    list = []

    # Checks if start letters match target letters and excludes letters that match.
    if (sum(1 for (c, t) in zip(start, target) if c == t)) > 0:
        # Compares new start word to target word and returns a list of common leters.
        commonletters  = [i for i, j in enumerate(zip(start, target)) if all(k == j[0] for k in j)]
    for i in range(len(start)):
        # Creates a pattern for letter positions that are not found yet.
        if i not in commonletters:
            # Adds matching words to 'list and processes patterns where letter is not found.
            list += build(start[:i] + "." + start[i + 1:], words, seen, list)

    # If no words in common, exit loop and process next item.
    if len(list) == 0:
        return False
            
    # Sort list alphabetically and by count of matches. If reversed, sort by letters in common first.
    list = sorted([(same(w, target), w) for w in list], reverse = short)
        
    for (match, item) in list:
        # If 'match' matches 'target' or off by one letter.
        if match >= len(target) - 1:
            if match == len(target) - 1:
                # Append 'item' to 'path' and exist loop, as last 'item' has been found.
                path.append(item)
            return True
        # Mark true for each 'item' in list to exclude the 'item' from following searches.
        seen[item] = True

    # Set initial word in list as start word to loop on.
    for (match, item) in list:
        # Append 'item' to 'path' and start recursive search.
        path.append(item)
        # Call self recursively.
        if find(item, words, seen, target, path):
            return True
        # Remove last 'item' in 'path' if no path found for 'item'.
        path.pop()


def validate_file(fname):
    """
    Function validates input files and returns string according to validity.
    """
    try:
        if os.stat(fname).st_size > 0:
            # If data in 'fname'
            return "0"
        else:
            return "File is empty. Please enter again"
    except OSError:
        return "Cannot find file. Please enter again"


def create_words_list(start, lines, not_allowed):
    """
    Function creates list for words not allowed.
    """
    words = list()
    for line in lines:
        w = line.rstrip()
        if len(w) == len(start):
            if (w == start) or (w not in not_allowed):
                words.append(w)
    return words


def validate_start_word(start, lines):
    """
    Function validates start word input and returns string according to validity.
    """
    if start.isalpha():
        # Start word has to be alphabetic
        if len(start) > 1:
            # Start word has to be more than one character
            if start in lines:
                # Start word has to be in 'lines'
                return "0"
            else:
                return "Start word not found. Please enter again"
        else:
            return "Start word must be alphabetic characters. Please enter again"
    else:
        return "Start word must be alphabetic characters. Please enter again"


def validate_target_word(start, target, words):
    """
    Function validates target word input and returns string according to validity.
    """
    if target.isalpha():
        # Target word has to be alphabetic
        if len(start) == len(target):
            # Target word has to be the same length as start word
            if start != target:
                # Target word cannot be the same as start word
                if target in words:
                    # Target word has to be in list of words
                    return "0"
                else:
                    return "Target word not found. Please enter again"
            else:
                return "Target word cannot be the same as start word. Please enter again"
        else:
            return "Target word has to be the same length as start word. Please enter again"
    else:
        return "Target word accepts letters only. Please enter again"


def validate_option(option):
    """
    Function validates option ('y' or 'n' input) and returns string according to validity.
    """
    yes_no = ['y', 'n']
    if len(option) == 1:
        # Option must be the length of one character
        if option.isalpha():
            # Option has to be alphabetic
            if option in yes_no:
                # Option has to be y or n
                if option == 'y':
                    return "y"
                else:
                    return "n"
            else:
                return "This is not an option. Please enter y or n"
        else:
            return "This is not an option. Please enter y or n"
    elif len(option) > 1:
        return "Too many characters. Please enter y or n"
    else:
        return "No input. Please enter y or n"

# Prompts input of dictionary file
while True:
    fname = (input("Enter dictionary name: ").lower()).strip()
    f_error = validate_file(fname)
    if f_error == "0":
        lines = (open(fname, 'r').read()).split()
        break
    else:
        print(f_error)

# Prompts input of file containing words not allowed
while True:
    option = (input("Would you like to supply a list of words that are not allowed? Please enter y or n: ").lower()).strip()
    option_error = validate_option(option)
    not_allowed = list()
    if option_error == "y":
        while True:
            fname = (input("Enter file name of words not allowed: ").lower()).strip()
            f_error = validate_file(fname)
            if f_error == "0":
                not_allowed  = (open(fname, 'r').read()).split()
                break
            else:
                print(f_error)
        break
    elif option_error == "n":
        not_allowed = list()
        break
    else:
        print(option_error)

# Prompts input of start word
while True:
    start = (input("Enter start word: ").lower()).strip()
    start_word_error = validate_start_word(start, lines)
    if start_word_error == "0":
        break
    else:
        print(start_word_error)

# Creates list of words that have the same length as the start word
words = create_words_list(start, lines, not_allowed)

# Prompts input of target word
while True:
    target = (input("Enter target word: ").lower()).strip()
    target_word_error = validate_target_word(start, target, words)
    if target_word_error == "0":
        break
    else:
        print(target_word_error)

# Prompts input 'y' for user to specify number of steps in path or input
path_length = int
short = False
length = False

while True:    
    chose_length = (input("""Enter 'y' to specify the length of path or 'n to disregard: """).lower()).strip()
    option_error = validate_option(chose_length)
    if option_error == "y":
        length = True
        break
    elif option_error == "n":
        break
    else:
        print(option_error)

if length:
    while True:
        try:
            path_length = int(input("""Please specify the length: """))
            break
        except ValueError:
            print("""This is not a number""")
            
else:
    while True:
        option = (input("Enter 'y' for the shortest path or 'n' for any path: ").lower()).strip()
        yes_no_error = validate_option(option)
        if yes_no_error == "y":
            short = True
            break
        elif yes_no_error == "n":
            break
        else:
            print(yes_no_error)
    

count = 0
path = [start]
seen = {start : True}
# Outputs length of path and path
if find(start, words, seen, target, path):
    path.append(target)
    print(len(path) - 1, path)
else:
    print("No path found")

