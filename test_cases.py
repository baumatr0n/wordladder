import unittest
from unittest import TestCase
from word_ladder import *

class TestSame(TestCase):

    def test1_matching_letters(self): # 0 matching letters
        item = 'leap'
        target = 'fold'
        self.assertEqual(same(item, target) 0)
    
    def test2_matching_letters(self): # 1 matching letters
        item = 'lead'
        target = 'gold'
        self.assertEqual(same(item, target) 1)

    def test3_matching_letters(self): # 2 matching letters
        item = 'load'
        target = 'gold'
        self.assertEqual(same(item, target) 2)

    def test4_matching_letters(self): # 3 matching letters
        item = 'goad'
        target = 'gold'
        self.assertEqual(same(item, target) 3)

    def test5_matching_letters(self): # 4 matching letters
        item = 'gold'
        target = 'gold'
        self.assertEqual(same(item, target) 4)


class TestBuild(TestCase):

    def test6_build(self):
        pattern = '.ate'
        words = ['load', 'kite', 'bite', 'ride'] # 0 matching
        seen = {'side' : True} # 0 seen
        list = ['frat', 'bird'] # 0 found
        self.assertEqual(build(pattern, words, seen, list), list())

    def test7_build(self):
        pattern = '.ite'
        words = ['load', 'kite', 'bite', 'ride'] # 2 matching
        seen = {'side' : True} # 0 seen
        list = ['frat', 'bird'] # 0 found
        self.assertEqual(build(pattern, words, seen, list), list('kite', 'bite'))

    def test8_build(self):
        pattern = '.ite'
        words = ['load', 'kite', 'bite', 'ride'] # 2 matching
        seen = {'kite' : True} # 1 seen
        list = ['frat', 'bird'] # 0 found
        self.assertEqual(build(pattern, words, seen, list), list('bite'))

    def test9_build(self):
        pattern = '.ite'
        words = ['load', 'site', 'mite', 'kite', 'bite'] # 2 matching
        seen = {'kite' : True} # 1 seen
        list = ['frat', 'bird', 'kite'] # 1 found
        self.assertEqual(build(pattern, words, seen, list, list('mite', 'bite')
        

class TestFind(TestCase):

    def test10_found(self): # True
        start = 'hide'
        words = ['side', 'site']
        seen = {'hide' : True}
        target = 'seek'
        path = ['hide']
        self.assertTrue(find(start, words, seen, target, path))
            
    def test11_not_found(self): # False
        start = 'hide'
        words = ['side', 'trap']
        seen = {'hide' : True}
        target = 'seek'
        path = ['hide']
        self.assertTrue(find(start, words, seen, target, path))


class TestValidateFile(TestCase):
                         
    def test12_file_valid(self): # Valid file
        fname = 'dictionary.txt'
        self.assertEqual(validate_file(fname), '0')
                         
    def test13_file_empty(self): # Empty file
        fname = 'empty_file.txt'
        self.assertEqual(validate_file(fname), 'File is empty. Please enter again')
    
    def test14_file_not_found(self): # File not found
        fname = 'no file'
        self.assertEqual(validate_file(fname), 'Cannot find file. Please enter again')
                         
    def test15_no_file(self): # No file input
        fname = ''
        self.assertEqual(validate_file(fname), 'Cannot find file. Please enter again')
        

class TestCreateWordsList(TestCase):
                         
    def test16_not_allowed_empty(self): # Empty file
        start = 'rate'
        lines = ['brag', 'chad', 'cete', 'rate']
        not_allowed = []
        self.assertEqual(create_words_list(start, lines, not_allowed), ['brag', 'chad', 'cete', 'rate'])
    
    def test17_start_word_included(self): # Start word included in file
        start = 'rate'
        lines = ['brag', 'chad', 'cete', 'rate']
        not_allowed = ['brag', 'rate']
        self.assertEqual(create_words_list(start, lines, not_allowed), ['chad', 'cete', 'rate'])
    
    def test18_no_matches(self): # No matches in file
        start = 'rate'
        lines = ['brag', 'chad', 'cete', 'rate']
        not_allowed = ['brit', 'mate']
        self.assertEqual(create_words_list(start, lines, not_allowed), ['brag', 'chad', 'cete', 'rate'])
        

class TestValidateStart(TestCase):
                        
    def test19_start_valid(self): # Start word found
        start = 'lead'
        lines = ['lead', 'load', 'goad']
        self.assertEqual(validate_start_word(start, lines), '0')
                         
    def test20_start_not_valid(self): # Start word not found
        start = 'leed'
        lines = ['lead', 'load', 'goad']
        self.assertEqual(validate_start_word(start, lines), 'Start word not found. Please enter again')
                         
    def test21_start_not_alpha(self): # Start word is not alphabetic
        start = '1ead'
        lines = ['lead', 'load', 'goad']
        self.assertEqual(validate_start_word(start, lines), 'Start word must be alphabetic characters. Please enter again')
                
    def test22_start_no_input(self): # No start input
        start = ''
        lines = ['lead', 'load', 'goad']
        self.assertEqual(validate_start_word(start, lines), 'Start word must be alphabetic characters. Please enter again')
    
    
class TestValidateTarget(TestCase):
                
    def test23_target_valid(self): # Target word found
        start = 'lead'
        target = 'load'
        excl_words = ['lead', 'load', 'goad']
        self.assertEqual(validate_target_word(start, target, excl_words), '0')
    

    def test24_target_not_valid(self): # Target not word found
        start = 'lead'
        target = 'loaf'
        excl_words = ['lead', 'load', 'goad']
        self.assertEqual(validate_target_word(start, target, excl_words), 'Target word not found. Please enter again')
                         
    def test25_target_matches_start(self): # Target word matches start word
        start = 'lead'
        target = 'lead'
        excl_words = ['lead', 'load', 'goad']
        self.assertEqual(validate_target_word(start, target, excl_words), 'Target word cannot be the same as start word. Please enter again')
                         
    def test26_target_one_char(self): # Target word is only one character
        start = 'lead'
        target = 'l'
        excl_words = ['lead', 'load', 'goad']
        self.assertEqual(validate_target_word(start, target, excl_words), 'Target word has to be the same length as start word. Please enter again')
    
    def test27_target_too_long(self): # Target word is too long
        start = 'lead'
        target = 'loading'
        excl_words = ['lead', 'load', 'goad']
        self.assertEqual(validate_target_word(start, target, excl_words), 'Target word has to be the same length as start word. Please enter again')
                         
    def test28_target_too_short(self): # Target word is too short
        start = 'lead'
        target = 'lad'
        excl_words = ['lead', 'load', 'goad']
        self.assertEqual(validate_target_word(start, target, excl_words), 'Target word has to be the same length as start word. Please enter again')
                         
    def test29_target_not_alpha(self): # Target word is not alphabetic
        start = 'lead'
        target = 'l3ad'
        excl_words = ['lead', 'load', 'goad']
        self.assertEqual(validate_target_word(start, target, excl_words), 'Target word accepts letters only. Please enter again')

    def test30_target_no_input(self): # No target input
        start = 'lead'
        target = ''
        excl_words = ['lead', 'load', 'goad']
        self.assertEqual(validate_target_word(start, target, excl_words), 'Target word accepts letters only. Please enter again')
        

class TestValidateOption(TestCase):

    def test31_option_valid_yes(self): # Option y is valid
        option = 'y'
        self.assertEqual(validate_option(option), 'y')
                         
    def test32_option_valid_no(self): # Option n is valid
        option = 'n'
        self.assertEqual(validate_option(option), 'n')

    def test33_option_not_yn(self): # Option is wrong letter
        option = 'x'
        self.assertEqual(validate_option(option), 'This is not an option. Please enter y or n')
                         
    def test34_option_number(self): # Option is not a letter
        option = '0'
        self.assertEqual(validate_option(option), 'This is not an option. Please enter y or n')

    def test35_option_many_letters(self): # Option is to many characters (letters)
        option = 'yy'
        self.assertEqual(validate_option(option), 'Too many characters. Please enter y or n')
    
    def test36_option_many_numbers(self): # Option is too many characters (numbers)
        option = '09'
        self.assertEqual(validate_option(option), 'Too many characters. Please enter y or n')
                         
    def test37_option_empty(self): # No option input
        option = ''
        self.assertEqual(validate_option(option), 'No input. Please enter y or n')
        
if __name__ == '__main__':
    unittest.main()